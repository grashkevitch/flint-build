#!/bin/bash
# Process git commands for all repos in a directory.

if [ $# -lt 1 ]; then
  echo "Usage: gittree <drush alias> <command>"
  exit
fi

ac_docroot=$1
git_command=${@:2}

local=${ac_docroot}'.local'
docroot=$(drush dd @${local})
working_folder=$(dirname ${docroot})

cd ${working_folder}

for gitdir in $(find . -type d -name .git -not -path "*/libraries*" -not -path "*/contrib*"); do
  # Display repository name in blue
  repo=$(dirname $gitdir)
  echo -e "\033[34m$repo\033[0m"

  # Run git command in the repositories directory
  cd $repo && git ${git_command}
  ret=$?

  # Return to calling directory (ignore output)
  cd - > /dev/null

  # Abort if cd or git command fails
  if [ $ret -ne 0 ]; then
    exit
  fi

  echo
done
