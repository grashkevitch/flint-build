Flint Scripts
=============

The following is a description of the different build and deployment processes
available for Flint.

Build Process Overview
----------------------

The build process performs the following steps:

1. Creates a directory at ../artifacts/${site}/${build_id}
2. Clones the required repositories.
3. Deletes the Acquia Cloud repo and remakes it empty with just its .git folder.
4. Copies in all global folders to the Acquia Cloud repo.
5. Runs drush make which pulls all site code into the docroot folder in the
Acquia Cloud repo.
6. Moves in all site specific modules, themes and configuration.
7. Commits the changes to the Acquia Cloud repo, tags it with the build id, and
then pushes it to remote.

While the build process can be run manually, Bamboo will be responsible for the
site variables required for the script and creating a build ID.

Build Process Details
---------------------

When performing a build, three major repos are pulled in:
- Flint site repo
- Acquia Cloud site repo
- Flint global assets

Each site on the Flint platform needs its own repository that contains:
- site.make - This file calls the global Flint make file and allows overrides.
- conf - Contains the settings.php file for the site.
- modules - Contains any site specific modules.
- themes - Contains any site specific themes.

The Acquia Cloud site repo contains the result of the build process.

The Flint global assets repo contains:
- flint.make - The make file that applies to all sites on the Flint platform.
- acquia-utils - Scripts for our hosting provider.
- library - Any extensions compiled by our team for our hosting.
- patches - Module patches that are applied during the build process.

In order to use build.sh, you need the following parameters:

1. build_id - The id to use for the build ex: 00012
2. site - The Acquia Cloud ID for the site ex: adskflint
3. site_repo_url - The URL for the Flint site repo ex: git@git.autodesk.com:Flint/flint-site-flint.git
4. ac_repo_url - The URL for the site specific Acquia Cloud repo. ex: adskflint@svn-6135.prod.hosting.acquia.com:adskflint.git

Full example: ./build.sh 00002 adskflint git@git.autodesk.com:Flint/flint-site-flint.git adskflint@svn-6135.prod.hosting.acquia.com:adskflint.git

Deployment Process Overview
---------------------------

The deployment process will check out the tag on the destination environment in
Acquia Cloud. It takes the following steps for production:

1. Site is put into maintenance mode.
2. Caches are flushed to reduce database size.
3. The database is backed up.
4. The build tag is deployed.
5. Database and configuration updates are run.
6. Prod and non-prod modules are enabled and disabled respectively.
7. Caching, CSS and JS aggregation are enabled.
8. Visible error reporting is disabled.
9. Site is taken out of maintenance mode.
10. Caches are flushed again along with Varnish.

For dev and stage environments, the process is slightly different:
1. Production database is copied to environment.
2. The build tag is deployed.
3. Database and configuration updates are run.
4. Prod and non-prod modules are disabled and enabled respectively.
5. Caching, CSS and JS aggregation are disabled.
6. Visible error reporting is enabled.
7. Caches are flushed again along with Varnish.

Deployment Process Details
--------------------------

To use deploy.sh, the following parameters are required:

1. build_id - The id of the build that is to be deployed. ex: 00012
2. docroot - The Acquia Cloud ID for the site ex: adskflint
3. target_env - The target environment to which to deploy. Includes dev, test,
prod. ex: prod

Local Process
-------------

A script local.sh has been provided for local development. It requires that a
drush alias at .local has been setup, and it performs the following operations:

1. Creates a build in the docroot defined in the local drush alias. It will
delete any code located in that docroot by default.
2. Copies the database from the dev environment to the local database.
3. Creates a local.settings.php file database credentials.

The script takes the following parameters:

1. type - The type of operation to perform. Allowed options are all, build and
deploy. This is useful if a new database is needed but not a new build.
2. ac_docroot - The Acquia Cloud ID for the site ex: adskflint
3. db_name - Name of the local database to use. ex: local_flint
4. db_user - The database user that has permissions for the local db. ex: root
5. db_pass - optional - The password for the database user. Empty string if
omitted.
6. db_host - optional - The host for the database server. Defaults to localhost.
ex: myserver.com

Git Tree
--------

This script will allow a git command to be passed to all subdirectories in a given drush alias.

To use gittree.sh the following parameters are required:
1. docroot - The Acquia Cloud ID for the site ex: adskflint
2. git command - The git command to run ex: pull origin master

Here are some example commands:

Status - ./gittree.sh adskspark status
Fetch all - ./gittree.sh adskspark fetch --all
Pull all - ./gittree.sh adskspark pull origin master
