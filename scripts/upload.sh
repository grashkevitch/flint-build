#!/bin/bash

build_id=$1

tar xfj ac_repo.tar.bz2
cd ac_repo
# Check that tag does not already exist
if [[ $(git ls-remote --tags | grep build_${build_id}) == *"build_${build_id}" ]]; then
  echo "Tag already exisits!"
  echo "Not uploading Artifact to Git again."
  echo "Going to next step in the Deploy..."
else
  # Tag changes and push.
  git config user.name "ACG Bamboo"
  git config user.email acg.web.ops@autodesk.com
  git add --all
  git commit -m "Bamboo Build ${build_id}"
  git tag build_${build_id}
  git push origin --tags
fi
