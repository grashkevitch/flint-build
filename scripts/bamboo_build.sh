#!/bin/bash
# Build script specific for Bamboo

# Script will exit immediately if any command fails
set -e

# Map the script inputs to convenient names.
ac_repo_url=$1

rm -rf ac_repo
# Clone site from Acquia Cloud.
git clone -b master ${ac_repo_url} ac_repo

# Move .git folder out of the way.
mv ac_repo/.git .git
rm -rf ac_repo
# Remake code directory and move back in .git folder.
mkdir ac_repo
mv .git ac_repo/.git

# Move in global folders.
cp -r flint_global/acquia-utils ac_repo/acquia-utils
cp -r flint_global/library ac_repo/library
cp -r flint_global/patches ac_repo/patches
cp -r flint_global/composer ac_repo/composer

# Build codebase.
/home/ubuntu/.composer/vendor/bin/drush make site/site.make ac_repo/docroot

# Copy in custom settings.php file.
cp site/conf/settings.php ac_repo/docroot/sites/default/settings.php
# Copy in common settings.php
cp flint_global/conf/common.settings.php ac_repo/docroot/sites/default/common.settings.php
# Move in site specific modules and themes keeping git repo in place.
cp -r site ac_repo/docroot/sites/all/site
# Remove .git folder so its files get commited
rm -rf ac_repo/docroot/sites/all/site/.git
# Create symlinks so Drupal knows they exist.
cd ac_repo/docroot/sites/all/modules
ln -s ../site/modules site
cd ../themes
ln -s ../site/themes site
cd ../../../../../
# Move in custom .htaccess
rm ac_repo/docroot/.htaccess
cp flint_global/.htaccess ac_repo/docroot/.htaccess

# Compile SASS.
# Find all conf files and run them

#create artifact
tar cfj ac_repo.tar.bz2 ac_repo
