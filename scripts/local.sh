#!/bin/bash
# Standard deployment script for Acquia Cloud

# Map the script inputs to convenient names.
# The type of process. Options are all, build, deploy. Defaults to all
type=$1
ac_docroot=$2
db_name=$3
db_user=$4
db_pass=$5
db_host=$6

local=${ac_docroot}'.local'
remote=${ac_docroot}'.dev'

docroot=$(drush dd @${local})
working_folder=$(dirname ${docroot})

# Check if password is set, if not use empty string.
db_pass=${db_pass:-""}
# Check if host is set. If not use localhost.
db_host=${db_host:-"127.0.0.1"}

# Set up variables based on docroot.
if [ ${ac_docroot} == "adskflint" ];
then
  site_repo_url="git@bitbucket.org:adskflint/flint-site-flint.git";
elif [ ${ac_docroot} == "adskember" ];
then
  site_repo_url="git@bitbucket.org:adskflint/flint-site-ember-printer.git";
elif [ ${ac_docroot} == "adskdevportal" ];
then
  site_repo_url="git@bitbucket.org:adskflint/flint-site-devportal.git";
elif [ ${ac_docroot} == "adskspark" ];
then
  site_repo_url="git@bitbucket.org:adskflint/flint-site-spark.git";
elif [ ${ac_docroot} == "adsktinkercad" ];
then
  site_repo_url="git@bitbucket.org:adskflint/flint-site-tinkercad.git";
elif [ ${ac_docroot} == "adskcircuits" ];
then
  site_repo_url="git@bitbucket.org:adskflint/flint-site-circuits.git";
fi

# Check if build is needed for this run.
if [ ${type} == "all" -o ${type} == "build" ];
then
  if [ -d ${working_folder} ];
  then
    echo "Be afraid! ${working_folder} already exists. Replace it? This will delete everything in the folder."
    select yn in Yes No;
    do
      case ${yn} in
        Yes ) rm -rf ${working_folder}; mkdir ${working_folder}; break;;
        No ) exit;;
      esac
    done
  else
    mkdir ${working_folder}
  fi

  echo "Starting build."
  cd ${working_folder}

  # Clone site configuration repo.
  git clone -b master ${site_repo_url} site
  # Clone flint repo.
  git clone -b master git@bitbucket.org:adskflint/flint-global.git flint_global

  # Move in global folders.
  cp -r flint_global/acquia-utils acquia-utils
  cp -r flint_global/library library
  cp -r flint_global/patches patches
  cp -r flint_global/composer composer

  # Build codebase.
  drush make --working-copy --no-patch-txt site/site.make docroot

  # Move in custom settings.php file.
  cp site/conf/settings.php docroot/sites/default/settings.php
  # Copy in common settings.php
  cp flint_global/conf/common.settings.php docroot/sites/default/common.settings.php
  # Move in site specific modules and themes keeping git repo in place.
  cp -r site docroot/sites/all/site
  # Create symlinks so Drupal knows they exist.
  cd docroot/sites/all/modules
  ln -s ../site/modules site
  cd ../themes
  ln -s ../site/themes site
  cd ../../../../
  # Move in custom .htaccess
  rm docroot/.htaccess
  cp flint_global/.htaccess docroot/.htaccess

  echo "<?php

  \$databases = array (
    'default' =>
    array (
      'default' =>
      array (
        'database' => '${db_name}',
        'username' => '${db_user}',
        'password' => '${db_pass}',
        'host' => '${db_host}',
        'port' => '',
        'driver' => 'mysql',
        'prefix' => '',
      ),
    ),
  );

  \$conf['imagemagick_convert'] = '/usr/local/bin/convert';

  " > docroot/sites/default/local.settings.php

  echo "Build complete."
fi

# Check if deployment is needed for this run.
if [ ${type} == "all" -o ${type} == "deploy" ];
then
  echo "Beginning deployment."
  #
  # Database operations.
  #

  echo "Copying database from stage environment"
  drush sql-sync @${remote} @${local}

  drush @${local} cc all

  #
  # Post code deployment operations.
  #
  echo "Running database updates."
  drush @${local} updb -y

  echo "Enabling Stage File Proxy module."
  drush @${local} en stage_file_proxy -y

  # Ensure caching is disabled for non-prod environments.
  echo "Disabling caching and aggregation."
  drush @${local} vset cache 0
  drush @${local} vset preprocess_js 0
  drush @${local} vset preprocess_css 0
  drush @${local} vset page_compression 0

  echo "Enabling visible error reporting."
  drush @${local} vset error_level 2

  echo "Reverting features."
  drush @${local} fra -y

  # Bring site online regardless of environment.
  echo "Disabling maintenance mode."
  drush @${local} vset maintenance_mode 0

  echo "Final cache clear."
  drush @${local} cc all

  echo "Copying files from dev"
  drush -y rsync @${remote}:%files/ @${local}:%files

  echo "Deployment complete."
fi
