#!/bin/bash
# Standard deployment script for Acquia Cloud

# Map the script inputs to convenient names.
build_id=$1
site=$2
site_repo_url=$3
ac_repo_url=$4

# Create directories.
mkdir "../artifacts/${site}"
mkdir "../artifacts/${site}/${build_id}"
cd "../artifacts/${site}/${build_id}"

# Clone site configuration repo.
git clone -b master ${site_repo_url} site
# Clone flint repo.
git clone -b master git@bitbucket.org:adskflint/flint-global.git flint_global
# Clone site from Acquia Cloud.
git clone -b master ${ac_repo_url} ac_repo

# Move .git folder out of the way.
mv ac_repo/.git .git
rm -rf ac_repo
# Remake code directory and move back in .git folder.
mkdir ac_repo
mv .git ac_repo/.git

# Move in global folders.
cp -r flint_global/acquia-utils ac_repo/acquia-utils
cp -r flint_global/library ac_repo/library
cp -r flint_global/patches ac_repo/patches
cp -r flint_global/composer ac_repo/composer

# Build codebase.
drush make site/site.make ac_repo/docroot

# Move in custom settings.php file.
cp site/conf/settings.php ac_repo/docroot/sites/default/settings.php
# Copy in common settings.php
cp flint_global/conf/common.settings.php docroot/sites/default/common.settings.php
# Move in site specific modules and themes keeping git repo in place.
cp -r site ac_repo/docroot/sites/all/site
# Move in custom .htaccess
rm ac_repo/docroot/.htaccess
cp flint_global/.htaccess ac_repo/docroot/.htaccess

# Compile SASS.
# Find all conf files and run them

# Tag changes and push.
cd ac_repo
git add -all
git commit -m "Build ${build_id}"
git tag build_${build_id}
git push origin --tags
