#!/bin/bash
# Standard deployment script for Acquia Cloud

# Map the script inputs to convenient names.
build_id=$1
docroot=$2
target_env=$3
drush_alias=${docroot}'.'${target_env}
drush_prod=${docroot}'.prod'

parse_var_export () {
  value=$(php -r "\$obj = $1; print \$obj->$2;")
  echo ${value}
}

task_check () {
  task_response="$(drush @${drush_alias} ac-task-info ${1} --format=var_export)"
  task_state=$(parse_var_export "${task_response}" state)
  if [ ${task_state} = "done" ];
  then
    echo 1;
  else
    echo 0;
  fi
}

flush_varnish() {
  # Purge all Varnish .com domains attached to environment
  # https://gist.github.com/heathdutton/5fd2fff9353b3f475cfb
  json="$(drush @${drush_alias} ac-domain-list --format=json)"
  if [ "$?" -ne "0" ]; then
    echo "The environment needs to have login permissions in order to purge Varnish."
    echo "Run the following to have the environment log in securely:"
    echo "    drush @$drush_alias ssh 'drush ac-api-login'"
  else
    domains="$(echo ${json} | tr '"' '\n' | grep .com)"
    for domain in ${domains[@]}
    do
      echo "    Flushing ${domain}"
      drush @${drush_alias} ac-domain-purge ${domain} > /dev/null
    done
  fi
}

echo "Beginning deployment script."

#
# Database preparation.
#
if [ ${target_env} = "prod" ];
then
  echo "Enabling maintenance mode."
  drush @${drush_alias} vset maintenance_mode 1

  # Clear caches to reduce DB size before backup.
  echo "Clearing caches."
  drush @${drush_alias} cc all

  echo "Backing up production database."
  task="$(drush @${drush_alias} ac-database-instance-backup ${docroot} --format=var_export)"
else
  echo "Backing up database."
  task="$(drush @${drush_alias} ac-database-instance-backup ${docroot} --format=var_export)"
  # Disabling pulling back a database until all docroots are on Flint. (ex: adskspark)
  #echo "Copying database from prod to target environment."
  #task="$(drush @${drush_prod} ac-database-copy ${docroot} ${target_env} --format=var_export)"
fi

task_id=$(parse_var_export "$task" id)
echo "Started database task ${task_id}"

# Wait for database operation to complete.
status="$(task_check ${task_id})"
while [ ${status} -eq 0 ]
do
  echo "Waiting on task ${task_id}."
  sleep 5;
  status="$(task_check ${task_id})"
done

echo "Completed task ${task_id}"

#
# Deploy a tag in the repo.
#
echo "Deploying build ${build_id}."
task="$(drush @${drush_alias} ac-code-path-deploy tags/build_${build_id} --format=var_export)"
task_id=$(parse_var_export "$task" id)
echo "Started code deployment task ${task_id}"

# Wait for database operation to complete.
status="$(task_check ${task_id})"
while [ ${status} -eq 0 ]
do
  echo "Waiting on task ${task_id}."
  sleep 5;
  status="$(task_check ${task_id})"
done

#
# Post code deployment operations.
#
echo "Running database updates."
drush @${drush_alias} updb -y
# Flush caches again to ensure cleared cache.
drush @${drush_alias} cc all

if [ ${target_env} = "prod" ];
then
  echo "Disabling Stage File Proxy module."
  drush @${drush_alias} dis stage_file_proxy -y

  # Disable dblog and enable syslog
  echo "Ensure database logging is disabled."
  drush @${drush_alias} dis dblog -y
  drush @${drush_alias} en syslog -y

  echo "Disabling visible error reporting."
  drush @${drush_alias} vset error_level 0

  # Ensure caching is enabled
  echo "Enabling caching and aggregation."
  drush @${drush_alias} vset cache 1
  drush @${drush_alias} vset preprocess_js 1
  drush @${drush_alias} vset preprocess_css 1
  drush @${drush_alias} vset page_compression 1
else
  echo "Enabling Stage File Proxy module."
  drush @${drush_alias} en stage_file_proxy -y

  # Ensure caching is disabled for non-prod environments.
  echo "Disabling caching and aggregation."
  drush @${drush_alias} vset cache 0
  drush @${drush_alias} vset preprocess_js 0
  drush @${drush_alias} vset preprocess_css 0
  drush @${drush_alias} vset page_compression 0

  echo "Enabling visible error reporting."
  drush @${drush_alias} vset error_level 2
fi

echo "Reverting features."
drush @${drush_alias} fra -y

# Bring site online regardless of environment.
echo "Disabling maintenance mode."
drush @${drush_alias} vset maintenance_mode 0

echo "Final cache clear."
drush @${drush_alias} cc all
drush @${drush_alias} cc all

echo "Flushing Varnish"
flush_varnish

echo "Deployment script complete."
